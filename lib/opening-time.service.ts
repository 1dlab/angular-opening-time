import { Injectable } from '@angular/core';

@Injectable()
export class OpeningTimeService {

  constructor() { }

  /**
   * Takes an object representing opening time in the facebook format i.e : 
   * {
   *   "mon_1_open": "07:00",
   *   "mon_1_close": "12:00",
   *   ...
   * }
   *
   * and constructs an OpeningTime object from it.
   *
   * Fields with format errors or that are not coherent will be ignored.
   */
  buildOpeningTime(timeFbFormat: any): OpeningTime {
    let openingTime = new OpeningTime();
    for (let dayKey in timeFbFormat) {
      /**
       * If the key is an open key e.g 'mon_1_open', we look for its matching
       * closing key, e.g 'mon_1_close' and add a new range.
       */
      let dayReg = /^(\w{3})_(\d)_open$/;
      let match = dayKey.match(dayReg);
      /* Only if it was a valid open tag, otherwise we skip */
      if (match) {
        let day = match[1];
        let num = match[2];
        let openTime = timeFbFormat[dayKey];
        let closeKey = `${day}_${num}_close`;
        let closeTime = timeFbFormat[closeKey];
        if (closeTime) {
          openingTime._addRange({
            dayKey: dayKey,
            timeStr: openTime
          }, {
            dayKey: closeKey,
            timeStr: closeTime
          });
        }
      } /* if (match) */
    } /* end for */
    return openingTime;
  }

}

/**
 * Convenient object to store opening times. This object is iterable, each iteration returns
 * a WeekRange object. The iteration is sorted by day of the week (starts on monday) and 
 * by opening hour within the same day.
 *
 * example:
 *
 * let openingTime = OpeningTimeService.build(timeFromFb);
 * for (range of openingTime) {
 *    console.log(`Lieu ouvert le ${range.day.fr} entre ${range.open.str} et ${range.close.hours}:${range.close.minutes}`);
 * }
 */
export class OpeningTime {

  /**
   * Stores the all opening time ranges, sorted by day.
   * Within a day, we must ensure that the two time ranges (if there are 2) are sorted,
   * with the one with the first opening hour in first position.
   *
   * We don't check that ranges don't overlap.
   */
  private days: Array<{dayOfWeek: string, rangeArray: Array<DayRange>}> = [
    { dayOfWeek: 'mon', rangeArray: [] },
    { dayOfWeek: 'tue', rangeArray: [] },
    { dayOfWeek: 'wed', rangeArray: [] },
    { dayOfWeek: 'thu', rangeArray: [] },
    { dayOfWeek: 'fri', rangeArray: [] },
    { dayOfWeek: 'sat', rangeArray: [] },
    { dayOfWeek: 'sun', rangeArray: [] },
  ];


  /** 
   *  Returns a boolean describing the opening status for the given date.
   */
  isOpen(now: Date): boolean {
    let weekDayConvert = {0: 'sun', 1: 'mon', 2: 'tue', 3: 'wed', 4: 'thu', 5: 'fri', 6: 'sat'};

    let nowDay: string = weekDayConvert[now.getDay()];
    let yesterdayDay: string = weekDayConvert[(now.getDay() + 7 - 1) % 7];
    let nowTimestamp: number = now.getHours() * 60 + now.getMinutes();
    
    /* Check if now is within today's ranges */
    let ranges: Array<DayRange> = this.days.find((day) => {
      return day.dayOfWeek == nowDay
    }).rangeArray;
    
    for (let i = 0; i < ranges.length; i++) {
      let range: DayRange = ranges[i];

      /* Regular range, if now is within the range, return true */
      if (nowTimestamp > range.open.timestamp && nowTimestamp < range.close.timestamp) {
        return true;
      }

      if (range.open.timestamp > range.close.timestamp) {
        /* Case of night range for today */
        if (nowTimestamp > range.open.timestamp) {
          return true;
        }
      }
    }

    /* Check night range for yesterday (in case now is still in yesterday range that spanned over midnight) */
    ranges = this.days.find((day) => {
      return day.dayOfWeek == yesterdayDay;
    }).rangeArray;
    for (let i = 0; i < ranges.length; i++) {
      let range: DayRange = ranges[i];
      if (range.open.timestamp > range.close.timestamp) {
        /* Case of night range for today */
        if (nowTimestamp < range.close.timestamp) {
          return true;
        }
      }
    }

    return false;
    
  };

  /**
   * Returns the day for which an opening range can be added. A day cannot have more than
   * two opening ranges, so the days that are already full are not returned.
   */
  getAvailableDays(): Array<Day> {
    return this.days.filter((row) => {
      return row.rangeArray.length < 2;
    }).map((row) => {
      return Day.getDay(row.dayOfWeek);
    });
  }

  /**
   * Adds an opening time range for the this OpeningTime objet. Hours and minutes can be provided 
   * as integer or as string representing an integer.
   *
   * Throws an exception if the day does not exist, or if the day is already full.
   *
   * @param day - Short string representing a day : 'mon', 'tue', etc. See "short" property of Day object
   * @param open.hours - Hours of open time
   * @param open.minutes - Minutes of open time
   * @param close.hours - Hours of close time
   * @param close.minutes - Minutes of close time
   */
  addSingleOpeningTime(day: string, _open: {hours: number|string, minutes: number|string}, _close: {hours: number|string, minutes: number|string}) {
    let open: {hours: number, minutes: number} = {hours: null, minutes: null};
    let close: {hours: number, minutes: number} = {hours: null, minutes: null};
    open.hours = (typeof _open.hours === 'string') ? _open.hours = parseInt(<string>_open.hours) : _open.hours;
    open.minutes = (typeof _open.minutes === 'string') ? _open.minutes = parseInt(<string>_open.minutes) : _open.minutes;
    close.hours = (typeof _close.hours === 'string') ? _close.hours = parseInt(<string>_close.hours) : _close.hours;
    close.minutes = (typeof _close.minutes === 'string') ? _close.minutes = parseInt(<string>_close.minutes) : _close.minutes;

    let row = this.days.find((r) => {
      return r.dayOfWeek === day;
    });

    if (!row) {
      throw new OpeningTimeError(`No day of week correspond to ${day}`);
    }
    let rangeArray = row.rangeArray;
    if (rangeArray.length >= 2) {
      throw new OpeningTimeError('There are already 2 ranges for this day');
    }

    /* Insert new range. If already one range, sort them */
    let newRange: DayRange = DayRange.fromData({open: open, close: close});
    if ((rangeArray.length) == 1 && (rangeArray[0].open.timestamp > newRange.open.timestamp)) {
      rangeArray.unshift(newRange);
    } else {
      rangeArray.push(newRange);
    }
  }

  /**
   * Remove a single opening time range from the OpeningTime object. Identified by the slug for the day
   * and the index of the range within the day (0 or 1), as given by WeekRange object
   */
  removeSingleOpeningTime(day: string, index: number) {
    this.days.find((r) => {
      return r.dayOfWeek === day;
    })
    .rangeArray.splice(index, 1);
  }
  

  /**
   * Returns an object in the facebook format : 
   *
   * {
   *   "mon_1_open": "07:00",
   *   "mon_1_close": "12:00",
   *   ...
   * }
   */
  toFbFormat(): any {
    return this.getRanges().reduce((acc, range) => {
      let strs = range.getFbFormatStrings();
      acc[strs[0]] = strs[1];
      acc[strs[2]] = strs[3];
      return acc;
    }, {});
  }

  /**
   * Throws an OpeningTimeError for the following reasons : 
   * - Forward errors from DayRange constructor
   * - Day already full (maximum 2 ranges per day)
   * - Day of week could not be inferred from the given string
   *
   * It's on the caller responsability to check that the given `open` and `close` object are coherent (i.e they represent
   * the same day), they won't be cheked.
   *
   * @param open - Object representing the opening time for the range to add. Directly taken from the FB format
   * @param open.dayKey - String key from the FB object format, example "mon_1_open"
   * @param open.timeStr - String value representing the opening time, example : "07:00"
   * @param close - Object representing the opening time for the range to add. Same format than the `open` parameter.
   *
   */
  _addRange(open: {dayKey: string, timeStr: string}, close: {dayKey: string, timeStr: string}) {
    let dayReg = /^(\w{3})_\d_open$/;
    /* get day string representation in short format, e.g 'mon' */
    let day: string = open.dayKey.match(dayReg)[1]; 
    /* get Range array for this day */
    let rangeArray: Array<DayRange> = this.days.find((i) => {
      return i.dayOfWeek == day;
    }).rangeArray;

    /* Check validity */
    if (!rangeArray) {
      throw new OpeningTimeError(`No day of week correspond to ${day}`);
    }
    if (rangeArray.length >= 2) {
      throw new OpeningTimeError('There are already 2 ranges for this day');
    }

    /* Insert new range. If already one range, sort them */
    let newRange: DayRange = DayRange.fromStrings({open: open.timeStr, close: close.timeStr});
    if ((rangeArray.length) == 1 && (rangeArray[0].open.timestamp > newRange.open.timestamp)) {
      rangeArray.unshift(newRange);
    } else {
      rangeArray.push(newRange);
    }
  }

  /**
   * For the iterator to properly work, we do not build a new range object each time, but only
   * if the object changed since last time.
   */
  private _ranges: Array<WeekRange>;
  getRanges(): Array<WeekRange> {
    /* We flatten this.days in an array of WeekRange objects */
    let newRanges: Array<WeekRange> = this.days.map((dayObj) => {
      let dayOfWeek = dayObj.dayOfWeek;
      return dayObj.rangeArray.map((range, index) => {
        return new WeekRange(dayOfWeek, range, index);
      })
    }).reduce(function(a, b) { /* flatten */
      return a.concat(b);
    });

    if (!this._ranges || !this._equalRanges(this._ranges, newRanges)) {
      this._ranges = newRanges;
    }

    return this._ranges;
  }
  _equalRanges(ranges1: Array<WeekRange>, ranges2: Array<WeekRange>): boolean {
    return ranges1.length === ranges2.length &&
      ranges1.reduce((acc, range, idx) => {
        return acc && range.equals(ranges2[idx]);
      }, true);
  }

  /**
   * Iterator to make OpeningTime iterable. Should return WeekRange objects,
   * sorted by day of week and opening hour within the day.
   */
  [Symbol.iterator](): any {
    let index = 0;
    let flattenedRanges = this.getRanges();

    return  {
      next: () => {
        if (index < flattenedRanges.length) {
          let value = flattenedRanges[index++];
          return {done: false, value: value};
        } else {
          return {done: true, value: null};
        }
      }
    }
  }
}


/**
 * Represent an opening time range within a day (the day itself is not
 * an information carried by this object). This object represents
 * an opening time followed by a closing time.
 */
export class DayRange {
  open: {hours: number, minutes: number, timestamp: number, str: string};
  close: {hours: number, minutes: number, timestamp: number, str: string};

  /**
   * Throws an OpeningTimeError for the following reasons :
   * - Bad formatted times
   * - Hours > 23 or minutes > 59
   * - Closing time before opening time
   *
   * @param opts - Options object with the following properties
   * @param opts.open - String representation of the opening hour in the format "hh:mm"
   * @param opts.close - String representation of the closing hour in the format "hh:mm"
   */
  static fromStrings(opts: {open: string, close: string}): DayRange {
    let reg = /^(\d{2}):(\d{2})$/;
    let openMatch = opts.open.match(reg);
    let openHours = parseInt(openMatch[1]);
    let openMinutes = parseInt(openMatch[2]);


    let closeMatch = opts.close.match(reg);
    let closeHours = parseInt(closeMatch[1]);
    let closeMinutes = parseInt(closeMatch[2]);

    return this.fromData({
      open: {hours: openHours, minutes: openMinutes}, 
      close: {hours: closeHours, minutes: closeMinutes}
    })
  }

  /**
   * Throws an OpeningTimeError for the following reasons :
   * - Hours > 23 or minutes > 59
   * - Closing time before opening time
   */
  static fromData(opts: {open: {hours: number, minutes: number}, close: {hours: number, minutes: number}}): DayRange {
    let openTimestamp = opts.open.hours * 60 + opts.open.minutes;
    let closeTimestamp = opts.close.hours * 60 + opts.close.minutes;

    if ((opts.open.hours > 23) || (opts.close.hours > 23)) {
      throw new OpeningTimeError('Hours can\'t be greater than 23');
    }
    if ((opts.open.minutes > 59) || (opts.open.minutes > 59)) {
      throw new OpeningTimeError('Minutes can\'t be greater than 59');
    }
    
    let openStr = ('00'+opts.open.hours).slice(-2) + ':' + ('00'+opts.open.minutes).slice(-2);
    let closeStr = ('00'+opts.close.hours).slice(-2) + ':' + ('00'+opts.close.minutes).slice(-2);
    let open = {
      hours: opts.open.hours,
      minutes: opts.open.minutes,
      timestamp: openTimestamp,
      str: openStr
    };
    let close = {
      hours: opts.close.hours,
      minutes: opts.close.minutes,
      timestamp: closeTimestamp,
      str: closeStr
    };
    return new this(open, close);
  }

  constructor(
    open: {hours: number, minutes: number, timestamp: number, str: string},
    close: {hours: number, minutes: number, timestamp: number, str: string}
  ) {
    this.open = open;
    this.close = close;
  }

  getOpenTimeStr(): string {
    return this.open.str;
  }

  getCloseTimeStr(): string {
    return this.close.str;
  }
}

/**
 * Represent an opening time range within a week.
 * This objects represents an opening time and a closing time for a given day.
 */
export class WeekRange {
  open: {hours: number, minutes: number, timestamp: number, str: string};
  close: {hours: number, minutes: number, timestamp: number, str: string};
  day: Day;
  /* Index of the range within the day, as days can have multiple range. This is used to identify 
   * this exact range */
  index: number;
  uid: string;

  constructor(day: string, range: DayRange, index: number) {
    this.open = Object.assign(range.open);
    this.close = Object.assign(range.close);
    this.day = Day.getDay(day);
    this.index = index;
    this.uid = this.day.short + this.index;
  }

  equals(range: WeekRange): boolean {
    return (this.uid === range.uid) && 
           (this.open.timestamp === range.open.timestamp) &&
           (this.close.timestamp === range.close.timestamp);
  }

  /* 
   * Returns in an array the strings used by the facebook format. E.g
   * ['mon_1_open', '08:00', 'mon_1_close', '12:00']
   */
  getFbFormatStrings(): Array<string> {
    let idx = this.index + 1;
    return [
      `${this.day.short}_${idx}_open`, 
      this.open.str,
      `${this.day.short}_${idx}_close`, 
      this.close.str
    ];
  }
}

export class Day {
  short: string;
  fr: string;

  static getDay(slug: string): Day {
    return this.daysStringFormats.find((d) => {
      return d.short === slug;
    });
  }

  /*
   * Stores for each day the different string representations 
   */
  private static daysStringFormats: Array<Day> = [{
    short: 'mon',
    fr: 'lundi'
  }, {
    short: 'tue',
    fr: 'mardi'
  }, {
    short: 'wed',
    fr: 'mercredi'
  }, {
    short: 'thu',
    fr: 'jeudi'
  }, {
    short: 'fri',
    fr: 'vendredi'
  }, {
    short: 'sat',
    fr: 'samedi'
  }, {
    short: 'sun',
    fr: 'dimanche'
  }]
}

class OpeningTimeError extends Error{}
