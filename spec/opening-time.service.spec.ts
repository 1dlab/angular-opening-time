import 'zone.js';
import 'reflect-metadata';
import {OpeningTimeService} from "../lib/opening-time.service";

describe("OpeningTimeService", () => {
  let openingTimeService = new OpeningTimeService();

  describe("OpeningTime", () => {
    describe("isOpen()", () => {
      let openingTime = openingTimeService.buildOpeningTime({
        "mon_1_open": "08:00",
        "mon_1_close": "12:00",
        "mon_2_open": "14:00",
        "mon_2_close": "18:00",
        "wed_1_open": "21:00",
        "wed_1_close": "02:00"
      });

      it ("returns true during first range of the day", () => {
        let now = new Date('Mon Sep 04 2017 09:00:00 GMT+0200 (CEST)');
        expect(openingTime.isOpen(now)).toEqual(true);
      });

      it ("returns true during second range of the day", () => {
        let now = new Date('Mon Sep 04 2017 15:00:00 GMT+0200 (CEST)');
        expect(openingTime.isOpen(now)).toEqual(true);
      });

      it ("returns true during a range that spans over midnight", () => {
        let now = new Date('Wed Sep 06 2017 23:00:00 GMT+0200 (CEST)');
        expect(openingTime.isOpen(now)).toEqual(true);
        let now2 = new Date('Thu Sep 07 2017 00:30:00 GMT+0200 (CEST)');
        expect(openingTime.isOpen(now2)).toEqual(true);
      });

      it ("returns false outside of a range", () => {
        let now = new Date('Mon Sep 04 2017 12:30:00 GMT+0200 (CEST)');
        expect(openingTime.isOpen(now)).toEqual(false);
      });
    });
  });
});
